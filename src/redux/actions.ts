import { MessageInterface } from "../interface/Message.interface";
import { CREATE_MESSAGE, HIDE_LOADER, SHOW_LOADER } from "./types";

export function createMessage(message: MessageInterface) {
    return{
        type: CREATE_MESSAGE,
        payload: message
    }
}

export function showLoader() {
    return {
        type: SHOW_LOADER
    }
}

export function hideLoader() {
    return {
        type: HIDE_LOADER
    }
}
