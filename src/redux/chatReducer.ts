import { MessageInterface } from "../interface/Message.interface";
import { CREATE_MESSAGE } from "./types";

interface actionInterface {
    payload: MessageInterface;
    type: string;
}

export function chatReducer(state = {message: [{}]}, action: actionInterface) {
    switch(action.type) {
        case CREATE_MESSAGE:
            return {
                ...state,
                message: state.message.concat([action.payload])
            };
        default: return state
    }
}
