import { HIDE_LOADER, SHOW_LOADER } from "./types"

const initialState = {
    loading: true
};

interface actionInterface {
    type: string;
    loading: boolean;
};


export const appReducer = (state = initialState, action: actionInterface) =>{
    switch(action.type){
        case SHOW_LOADER:
            return {...state, loading: true}
        case HIDE_LOADER:
            return {...state, loading: false}
        default: return state
    }
}