import React, { ReactNode } from 'react';
import Header from './../Header';


import styles from './Layout.module.scss';
interface LayoutInterface {
    children: ReactNode
}

export default function Layout({ children }: LayoutInterface):JSX.Element {
    return(
        <div className={styles.wrapper}>
            <Header/>
            {children}
        </div>
    )
}