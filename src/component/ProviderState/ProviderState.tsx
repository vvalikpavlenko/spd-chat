import { ReactNode } from "react";
import { Provider } from "react-redux";
import { applyMiddleware, compose, createStore } from "redux";
import thunk from "redux-thunk";

import { rootReducer } from "../../redux/rootReducer";


const store = createStore(rootReducer, compose(
    applyMiddleware(thunk)
  ));

interface ProviderStateInterface{
    children: ReactNode
}

export default function ProviderState({ children }: ProviderStateInterface): JSX.Element {
    return(
        <Provider store={store}>
            {children}
        </Provider>
    )
}