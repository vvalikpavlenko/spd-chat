import { connect, useDispatch } from 'react-redux'
import { useForm } from "react-hook-form";
import classNames from 'classnames';
import { getDatabase, ref, push, set } from "firebase/database";
  
import { MESSAGES_TO_REPLY } from '../../redux/constants';
import { createMessage, showLoader } from "../../redux/actions";

import styles from './FormMessage.module.scss';
interface FormInputs {
    message: string;
}

function FormMessage() {
    const db = getDatabase();

    const dispatch = useDispatch();
    const {
        register,
        formState: { errors },
        handleSubmit,
        setValue
    } = useForm<FormInputs>();


    const onSubmit = ({message}: FormInputs) => {
        const newMessage = {
            text: message,
            isCurrentUser: true,
            id: Date.now().toString()
        }
        const postListRef = ref(db, 'chat/message');
        const newPostRef = push(postListRef);
        set(newPostRef, newMessage);

        dispatch(createMessage(newMessage));
        setValue('message', '');
        onMessageReply();
    };

     const onMessageReply = () => {
        dispatch(showLoader());
        setTimeout(() => {
            const randomText = Math.floor(Math.random() * MESSAGES_TO_REPLY.length);
            const text = MESSAGES_TO_REPLY[randomText]
            const newMessage = {
                text,
                isCurrentUser: false,
                id: Date.now().toString()
            };
            const postListRef = ref(db, 'chat/message');
            const newPostRef = push(postListRef);
            set(newPostRef, newMessage);
        }, 1000);
    };


    return(
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className={styles.form__plate}>
            <textarea 
                className={styles.form__textarea}
                {...register("message", { required: true })}
            />
            {errors.message && <p className={styles.form__error}>This is required message</p>}
            </div>
            <div className={classNames(styles.form__plate, styles['form__plate-button'])}>
            <button className={styles.form__button} type="submit">Send message</button>
            </div>
        </form>
    );
}


const napDispatchToProps = {
    createMessage
}

export default connect(null, napDispatchToProps)(FormMessage);