import styles from './Header.module.scss';

export default function Header(): JSX.Element { 
    return(
        <header className={styles.Header}>
            <h1 className={styles.Header__title}>SPDU</h1>
        </header>
    )
}