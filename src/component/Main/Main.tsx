import { getDatabase, ref, onValue } from 'firebase/database';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { MessageInterface } from '../../interface/Message.interface';
import { hideLoader } from '../../redux/actions';

import FormMessage from "../FormMessage/FormMessage";
import Loader from '../Loader';
import Message from "../Message";

import styles from './Main.module.scss';

interface stateInterface {
    app: {
        loading: boolean
    }
}

export default function Main():JSX.Element {
    const db = getDatabase();
    const dispatch = useDispatch();

    const loading = useSelector<stateInterface>(state => state.app.loading);
        
    const [message, setMessage] = useState<Array<MessageInterface>>();
    
    useEffect(()=>{
        const starCountRef = ref(db, 'chat/message');
        onValue(starCountRef, (snapshot) => {
        const data = snapshot.val();
        setMessage(Object.values(data));
        dispatch(hideLoader());
    });
    }, [db, dispatch]);
    
    return(
        <div className={styles.Main}>
            <div className="main__messages">
                {message && message.map(({text, isCurrentUser, id} : MessageInterface) => (
                    <Message key={id} text={text} isCurrentUser={isCurrentUser}/>
                ))}
                {!!loading && (
                    <Loader/>
                )}
            </div>
            <FormMessage/>
        </div>

    )
}
