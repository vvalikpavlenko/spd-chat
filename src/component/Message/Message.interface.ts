import { DetailedHTMLProps, HTMLAttributes } from "react";

export interface MessageComponentInterface extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>{
    text: string;
    isCurrentUser: boolean
}