import classNames from 'classnames';

import { MessageComponentInterface } from "./Message.interface";

import styles from './Message.module.scss';

const CURRENT_USER_AVATAR_URL = 'https://randomuser.me/api/portraits/lego/1.jpg';
const SECOND_USER_AVATAR_URL = 'https://randomuser.me/api/portraits/lego/2.jpg';

export default function Message({isCurrentUser, text}: MessageComponentInterface):JSX.Element {
    return(
        <div className={classNames(
            styles.Message, {
                [styles['Message-reverse']]: !isCurrentUser
            }
        )}>
            <img
                className={styles.Message__avatar}
                src={isCurrentUser ? CURRENT_USER_AVATAR_URL : SECOND_USER_AVATAR_URL}
                alt="lego"
            /> 
            <p className={styles.Message__bubble}>
                {text}
            </p>

        </div>
    )
}