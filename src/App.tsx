import Layout from "./component/Layout/Layout";
import Main from "./component/Main/Main";

function App() {
  return (
    <Layout>
      <Main/>
    </Layout>
  );
}

export default App;
