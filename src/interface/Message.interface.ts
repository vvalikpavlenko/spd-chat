
export interface MessageInterface {
    text: string;
    isCurrentUser: boolean;
    id: string;
}