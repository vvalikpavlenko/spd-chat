import { MessageInterface } from "./Message.interface";

export interface StateInterface{
    chat: {
        message?: Array<MessageInterface>;
    };
    app: {
        loader: boolean;
    };
}